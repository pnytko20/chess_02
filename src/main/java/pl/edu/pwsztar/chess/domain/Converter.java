package pl.edu.pwsztar.chess.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;

@AllArgsConstructor
@Getter
public class Converter {

    private int xSource;
    private int ySource;

    private int xDestination;
    private int yDestination;

    public Converter(FigureMoveDto figureMoveDto) {
        String source = figureMoveDto.getSource();
        String destination = figureMoveDto.getDestination();
        this.xSource = (int) source.charAt(0) - 96;
        this.ySource = Integer.parseInt(source.substring(2));
        this.xDestination = (int) destination.charAt(0) - 96;
        this.yDestination = Integer.parseInt(destination.substring(2));
    }

    public int getX() {
        return xSource;
    }

    public int getY() {
        return ySource;
    }

    public int getXDestination() {
        return xDestination;
    }

    public int getYDestination() {
        return yDestination;
    }

    @Override
    public String toString() {
        return "Converter{" +
                "x_source = " + xSource +
                ", y_source = " + ySource +
                ", x_dest = " + xDestination +
                ", y_dest = " + yDestination +
                '}';
    }
}