package pl.edu.pwsztar.chess.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.domain.Converter;
import pl.edu.pwsztar.chess.domain.Point;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.service.ChessService;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame rook;

    public ChessServiceImpl() {
        bishop = new RulesOfGame.Bishop();
        knight = new RulesOfGame.Knight();
        rook = new RulesOfGame.Rook();
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        Converter pointConverter = new Converter(figureMoveDto);

        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(new Point(pointConverter.getX(), pointConverter.getY()), new Point(pointConverter.getXDestination(), pointConverter.getYDestination()));
            case KNIGHT:
                return knight.isCorrectMove(new Point(pointConverter.getX(), pointConverter.getY()), new Point(pointConverter.getXDestination(), pointConverter.getYDestination()));
            case ROOK:
                return rook.isCorrectMove(new Point(pointConverter.getX(), pointConverter.getY()), new Point(pointConverter.getXDestination(), pointConverter.getYDestination()));
        }
        return false;
    }
}
